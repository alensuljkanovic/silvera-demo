import requests

GATEWAY_URL = "http://localhost:9095/api"

USER_SERVICE_URL = GATEWAY_URL + "/u"
BOOKSTORE_SERVICE_URL = GATEWAY_URL + "/b"
ORDER_SERVICE_URL = GATEWAY_URL + "/o"


USERS = [
    {"username":"user1", "password": "pass1", "email":"test1@tester.com"},
    {"username":"user2", "password": "pass2", "email":"test2@tester.com"},
    {"username":"user3", "password": "pass3", "email":"test3@tester.com"}
]

BOOKS = [
    {"isbn":"978-0-679-72020-1", "title":"L'Étranger", "author": "Albert Camus", "category": "Philosophical novel", "price": 20.1},
    {"isbn":"978-0-140-18020-6", "title":"La Peste", "author": "Albert Camus", "category": "Philosophical novel", "price": 10.46},
    {"isbn":"045-1-530-06-3", "title":"Преступленіе и наказаніе", "author": "Fyodor Dostoevsky", "category": "Philosophical novel", "price": 10.46}
]

ORDERS = [
    {
        "user": "user1",
        "items": [{"bookId":"978-0-679-72020-1", "amount":1},
                  {"bookId":"978-0-140-18020-6", "amount":1}]
    },
    {
        "user": "user2",
        "items": [{"bookId": "045-1-530-06-3", "amount":2},
                  {"bookId":"978-0-140-18020-6", "amount":1}]
    },
    {
        "user": "user3",
        "items": [{"bookId": "978-0-679-72020-1", "amount":3}]
    },
]

headers =  {"Content-Type":"application/json"}


def _debug_print(info, debug=False):
    if debug:
        print(info)


def _validate_results(data_sent, data_recv, debug=False, skip_fields=None):
    if skip_fields is None:
        skip_fields = []

    if not isinstance(data_recv, (list, dict)):
        return data_sent == data_recv

    # data_recv has additional fields, so we check the values of fields that
    # exist in both dicts.
    for k in data_sent:
        if k in skip_fields:
            continue

        _debug_print("Comparing: {}".format(k), debug)
        _debug_print("Elem1: {}".format(data_sent[k]), debug)
        _debug_print("Elem2: {}".format(data_recv[k]), debug)
        if isinstance(data_sent[k], list):
            # for d in data_sent[k]:
            #     v = _validate_results(data_sent[k], data_recv[k])
            pass
        elif data_sent[k] != data_recv[k]:
            return False
    return True

def post(service_url, data, debug=False, skip_fields=None):
    _debug_print("POST: %s" % service_url, debug)
    response = requests.post(service_url, json=data)
    result = response.json()
    _debug_print(result, debug)

    return _validate_results(data, result, skip_fields)

def put(service_url, id_field, data, debug=False):
    service_url = service_url + "/" + id_field
    _debug_print("PUT: %s" % service_url)

    response = requests.put(service_url, json=data)
    result = response.json()
    _debug_print(result, debug)

    return _validate_results(data, result)


def get(service_url, id_field, expected, debug=False, expected_type=None):
    service_url = service_url + "/" + id_field
    _debug_print("GET: %s" % service_url, debug)
    response = requests.get(service_url)
    try:
        result = response.json()
    except:
        # If function returns string
        if expected_type:
            result = expected_type(response.text)
        result = response.text
    _debug_print(result, debug)

    return _validate_results(expected, result)

def list_all(service_url, expected, debug=False):
    response = requests.get(service_url)
    result = response.json()
    _debug_print(result, debug)

    if len(expected) != len(result):
        return False

    # TODO: Compare content (can be a problem with el order)
    # for idx, d in enumerate(expected):
    #     res_d = result[idx]
    #     if not _validate_results(d, res_d):
    #         return False

    return True


def exists(service_url, expected, debug=False):
    response = requests.get(service_url)
    result = response.json()
    _debug_print(result, debug)

if __name__ == "__main__":

    # db.getMongo().getDBNames().filter(n => !['admin','local','config'].includes(n)).forEach(dname => db.getMongo().getDB(dname).dropDatabase());

    test_num = 0
    test_correct = 0

    # Add users
    print("Testing user: add")
    for user in USERS:
        test_num += 1
        if post(USER_SERVICE_URL + "/user", user):
            test_correct += 1

    # Update user emails
    print("Testing user: update")
    USERS[0]["email"] = "admin@tester.com"
    USERS[1]["email"] = "developer@tester.com"
    for user in USERS:
        test_num += 1
        if put(USER_SERVICE_URL + "/user", user["username"], user):
            test_correct += 1

    print("Testing user: listusers")
    test_num += 1
    if list_all(USER_SERVICE_URL + "/listusers", expected=USERS):
        test_correct += 1

    print("Testing user: exists")
    for user in USERS:
        test_num += 1
        if get(USER_SERVICE_URL + "/userexist", user["username"], expected=True):
            test_correct += 1

    print("Testing user: email")
    for user in USERS:
        test_num += 1
        if get(USER_SERVICE_URL + "/useremail", user["username"], expected=user["email"]):
            test_correct += 1

    #
    # BOOKS
    #

    # Add books into Bookstore
    print("Testing Book: add")
    for book in BOOKS:
        test_num += 1
        if post(BOOKSTORE_SERVICE_URL + "/book", book):
            test_correct += 1

    # Update titles to English and change prices
    print("Testing Book: update")
    BOOKS[0]["title"] = "The Stranger"
    BOOKS[0]["price"] = 14.39
    BOOKS[1]["title"] = "The Plague"
    BOOKS[1]["price"] = 12.59
    BOOKS[2]["title"] = "Crime and Punishmente"
    BOOKS[2]["price"] = 24.99
    for book in BOOKS:
        test_num += 1
        if put(BOOKSTORE_SERVICE_URL + "/book", book["isbn"], book):
            test_correct += 1

    # Get book by book by ID
    print("Testing Book: get")
    for book in BOOKS:
        test_num += 1
        if get(BOOKSTORE_SERVICE_URL + "/book", book["isbn"], expected=book):
            test_correct += 1

    print("Testing user: listbooks")
    test_num += 1
    if list_all(BOOKSTORE_SERVICE_URL + "/listbooks", expected=BOOKS):
        test_correct += 1

    print("Testing user: bookExists")
    for book in BOOKS:
        test_num += 1
        if get(BOOKSTORE_SERVICE_URL + "/bookexists", book["isbn"], expected=True):
            test_correct += 1

    print("Testing user: bookPrice")
    for book in BOOKS:
        test_num += 1
        if get(BOOKSTORE_SERVICE_URL + "/bookprice", book["isbn"], expected=book["price"], expected_type=float):
            test_correct += 1


    #
    # ORDERS
    #
    print("Testing Order: add")
    for order in ORDERS:
        test_num += 1
        if post(ORDER_SERVICE_URL + "/order", order, skip_fields=["price"]):
            test_correct += 1

    print("Testing Order: listorders")
    ORDERS[0]["price"] = 26.98
    ORDERS[1]["price"] = 62.57
    ORDERS[2]["price"] = 43.17
    test_num += 1
    if list_all(ORDER_SERVICE_URL + "/listorders", expected=ORDERS):
        test_correct += 1

    # Stats
    print("Tests num:", test_num)
    passed_stat =  (test_correct/test_num)*100
    print("Tests passed: {} ({}%)".format(test_correct, passed_stat))